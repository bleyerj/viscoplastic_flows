# viscoplastic_flows #

viscoplastic_flows is distributed as supplemental material to the paper

*Advances in the simulation of viscoplastic fluid flows using interior-point methods*, J. Bleyer, Computer Methods in Applied Mechanics and Engineering

This repository is distributed to provide a minimal working implementation and examples of the methods discussed in the paper.

### Contents

Python scripts based on the FEniCS project implementing:

* **Augmented Lagrangian methods** (including accelerated versions) for the antiplane case

* **Interior point solver** (second-order cone programming) for the antiplane case

If you are interested in 2D or 3D scripts, please send an email to [jeremy.bleyer@enpc.fr](mailto:jeremy.bleyer@enpc.fr).
   
### Citing

If you use these scripts in your work we would ask that you consider citing our paper.

```
@article {bleyer2017viscoplastic_ipm,
author = {Bleyer, Jeremy},
title = {Advances in the simulation of viscoplastic fluid flows using interior-point methods},
journal = {Computer Methods in Applied Mechanics and Engineering},
year = {2017},
}
```
along with citing the source code
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1038520.svg)](https://doi.org/10.5281/zenodo.1038520)

### How do I get set up? ###

You will just need a working FEniCS installation with Python. 
See the webpage of the FEniCS project for all information about download and installation https://fenicsproject.org/

### Issues and Support

For support or questions please email to [jeremy.bleyer@enpc.fr](mailto:jeremy.bleyer@enpc.fr).

### Authors

Bleyer Jeremy, Ecole des Ponts ParisTech, Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205), Université Paris-Est, France

### License

viscoplastic_flows is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
