# -*- coding: utf-8 -*-
"""
Conic programming module containing operations on variable belonging to a second-order Lorentz cone

This file is part of viscoplastic_flows based on FEniCS project (https://fenicsproject.org/)

viscoplastic_flows (c) by Jeremy Bleyer, Ecole des Ponts ParisTech, 
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)

viscoplastic_flows is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
"""
_author__ = "Jeremy Bleyer"
__license__ = "CC BY-SA 4.0"
__email__ = "jeremy.bleyer@enpc.fr"

from fenics import *
import numpy as np

class Cone:
    def __init__(self,dim=1):
        self.dim = dim
        self.Q = Identity(self.dim)
    def norm(self,x):
        return dot(x,dot(self.Q,x))
        
class Quad(Cone):
    """
    Second-order cone class containing Todd-Nesterov scaling operators
        see : Andersen, E. D., Roos, C., and Terlaky, T. (2003). On implementing a primal-dual interior-point method
        for conic quadratic optimization. Mathematical Programming, 95(2):249–277
    """
    def __init__(self,dim=1):
        self.dim = dim
        self.e = as_vector([1]*(self.dim-1))
        self.e0 = as_vector([1]+[0]*(self.dim-1))
        self.en = as_vector([0]+[1]*(self.dim-1))
        self.Q = diag(self.e0 - self.en)
        # tail
        self.A = as_matrix(np.hstack((np.zeros((dim-1,1)),np.eye(dim-1))))
    def tail(self,x):
        """Compute tail-part of a vector"""
        return dot(self.A,x)
    def mat(self,x):
        """Matrix (arrowhead) representation of a vector"""
        return x[0]*outer(self.e0,self.e0) + outer(self.e0,x-x[0]*self.e0) + outer(x-x[0]*self.e0,self.e0) + x[0]*diag(self.en)
    def circ(self,x,s):
        """Circular dot product"""
        return dot(self.mat(x),s)
    def imat(self,x):
        """Inverse of matrix representation"""
        d = self.norm(x)
        return 1./d*( x[0]*outer(self.e0,self.e0) - 
                      outer(self.e0,x-x[0]*self.e0) - 
                      outer(x-x[0]*self.e0,self.e0) + 
                      (d*diag(self.en) + outer(x-x[0]*self.e0,x-x[0]*self.e0))/x[0])
    def theta(self,x,s):
        """theta scaling variable"""
        return sqrt(sqrt(self.norm(s)/self.norm(x)))
    def w(self,x,s):
        """w scaling variable"""
        t = self.theta(x,s)
        a = sqrt(self.norm(s)*self.norm(x))
        return (s/t + t*dot(self.Q,x))/sqrt(2.)/sqrt(dot(x,s)+a)
    def W(self,w):
        """W scaling matrix"""
        return -self.Q + outer(self.e0 + w,self.e0 + w)/(1+dot(self.e0,w))  
    def F(self,w,t):
        """F scaling matrix"""
        return t*self.W(w)  
    def iF(self,w,t):
        """Inverse of F"""
        return 1/t*dot(self.Q,dot(self.W(w),self.Q))
    def iF2(self,w,t):
        """F**(-2)"""
        return 1./t**2*(-self.Q + 2*outer(dot(self.Q,w),dot(self.Q,w)))
    def iF2_tail(self,w,t):
        """Bottom-right (n-1)x(n-1) block (tail) of F**(-2)"""
        return 1./t**2*(Identity(self.dim-1) + 2*outer(self.tail(w),self.tail(w)))
    def F2_tail(self,w,t):
        """Inverse of bottom-right (n-1)x(n-1) block (tail) of F**(-2)"""
        return t**2*(Identity(self.dim-1) - 2/(1+2*dot(self.tail(w),self.tail(w)))*outer(self.tail(w),self.tail(w)))
    def v(self,x,s):
        """v scaling point"""
        w = self.w(x,s)
        t = self.theta(x,s)
        return dot(self.F(w,t),x)        
    def iW(self,w):
        """Inverse of W"""
        return dot(self.Q,dot(self.W(w),self.Q))