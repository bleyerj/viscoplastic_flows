"""
Augmented Lagrangian algorithms (including accelerated versions) 
for antiplane viscoplastic flow problems

This file is part of viscoplastic_flows based on FEniCS project (https://fenicsproject.org/)

viscoplastic_flows (c) by Jeremy Bleyer, Ecole des Ponts ParisTech, 
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)

viscoplastic_flows is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
"""
_author__ = "Jeremy Bleyer"
__license__ = "CC BY-SA 4.0"
__email__ = "jeremy.bleyer@enpc.fr"



from fenics import *
from mshr import *
import numpy as np
from time import time

set_log_level(ERROR)


# Problem parameters
eta, tau0 = Constant(1.), Constant(0.1)
f = Constant(1.)

# Algorithm parameters
R = Constant(eta)   # augmentation parameter
niter = 1000        # max number of iterations
tol = 1e-7          # convergence tolerance
algo = "ADMM"       # algorithm type ('ADMM' or 'AMA')
accelerated = False # accelerated version or not

# output options
plot_fields = True
output_results = False

if accelerated:
    method = "acc_"+algo
else:
    method = algo

# Some useful functions
eps = lambda x: grad(x)    
ppos = lambda x: (abs(x)+x)/2.

        
# annulus
path = "augmented_lagrangian/"
prefix = "annulus_0_04"
domain = Circle(Point(0,0),1.,segments = 100) - Circle(Point(0.04,0.),0.4,segments = 100) - Rectangle(Point(-1,0),Point(1,-1))
mesh = generate_mesh(domain, 50)

dim = mesh.topology().dim()    # Geometric dimension
dx = Measure("dx")
dx0 = Measure("dx",metadata={"quadrature_rule":"vertex","quadrature_degree":1})
h_cell = CellVolume(mesh)

# Output file
if output_results:
    results = XDMFFile(mpi_comm_world(),path + method + "_" + prefix + ".xdmf")
    results.parameters["flush_output"] = True
    
# Create function space
deg_u = 1
deg_strain = deg_u - 1
V = FunctionSpace(mesh, "CG", deg_u)
V0 = VectorFunctionSpace(mesh,"DG",deg_strain)
# Define functions
d = Function(V0,name="strain_rate")
d_old = Function(V0)
s = Function(V0,name="stress")
s_pred = Function(V0)
s_old = Function(V0)
u_pred = Function(V)
u_old = Function(V)
du = TrialFunction(V)
u_ = TestFunction(V)
u = Function(V,name="Velocity")

# Define bilinear form of Stokes-like step
a = inner(R*eps(du),eps(u_))*dx
# Define linear form
l = (dot(f,u_) + inner((R*d-s_pred),eps(u_)))*dx


def my_project(v,V,func=True):
    """Project function which enable pointwise operations for discontinuous functions"""
    u_ = TestFunction(V)
    if deg_strain == 0:
        nP = 1
    elif deg_strain == 1:
        nP = 3
    r = assemble(nP/h_cell*inner(v,u_)*dx0)
    if func:
        u = Function(V)
        u.vector().set_local(r.array())
        u.vector().apply("insert")   
        return u
    else:
        return r.array()
        
def border(x, on_boundary):
    return not(near(x[1],0)) and on_boundary
    
bc = [DirichletBC(V,Constant(0.), border)]

# Solver options-factorization is reused at each iteration since a is not changing
solver = LUSolver("mumps")
solver.parameters["symmetric"] = True
solver.parameters["reuse_factorization"] = True
K,res = assemble_system(a,l,bc)

# Initialization
norm = 1.
t = 1.
curr_time = 0
residual_log = np.zeros((niter,2))
for i in range(niter):
    tic = time()
    
    ## PROJECTION STEP
    if algo == "AMA":
        ns = sqrt(inner(s_pred,s_pred))
        p = conditional(lt(tau0,ns),s_pred/ns/eta*(ns-tau0),0*sig)
        d.assign(my_project(p,V0))
    elif algo == "ADMM":
        sig = s_pred + R*eps(u_pred)
        nsig = sqrt(inner(sig,sig))
        p = conditional(lt(tau0,nsig),sig/nsig/(eta+R)*(nsig-tau0),0*sig)
        d.assign(my_project(p,V0))
    
    ## STOKES-LIKE STEP    
    res = assemble(l)
    for bci in bc:
        bci.apply(res)
    solver.solve(K, u.vector(), res)
    
    ## UPDATE RESIDUAL AND LAGRANGE MULTIPLIER
    residual = my_project(eps(u) - d,V0)
    s.assign(s_pred + R*residual)
    norm = sqrt(assemble(inner(residual,residual)*dx))
    
    ## UPDATE WEIGHTS FOR ACCELERATED VERSION
    if accelerated:
        t_new = (1.+sqrt(1+4*t**2))/2.
    else:
        t_new = 1.
    
    ## UPDATE VARIABLES
    s_pred.assign(s + (t-1)/t_new*(s-s_old))
    u_pred.assign(u + (t-1)/t_new*(u-u_old))
    s_old.assign(s)
    u_old.assign(u)
    t = t_new
    
    ## STORE ITERATION INFO   
    curr_time += time()-tic
    residual_log[i,0] = norm
    residual_log[i,1] = curr_time
    print "Residual L2 norm : %g" % norm
    
    if plot_fields:
        plot(u,mode="color")
    ## CHECK FOR CONVERGENCE
    if norm < tol:
        print "Converged in %i iterations" % i        
        break

# output results
if output_results:
    results << (u,float(i))
    results << (d,float(i))
    results << (s,float(i))
    np.savetxt(path + method + "_" + prefix + "_residual_norm.csv", residual_log)

interactive()
