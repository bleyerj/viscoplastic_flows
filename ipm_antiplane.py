# -*- coding: utf-8 -*-
"""
Interior-point solver for antiplane viscoplastic flows (example of an eccentric annulus)

This file is part of viscoplastic_flows based on FEniCS project (https://fenicsproject.org/)

viscoplastic_flows (c) by Jeremy Bleyer, Ecole des Ponts ParisTech, 
Laboratoire Navier (ENPC,IFSTTAR,CNRS UMR 8205)

viscoplastic_flows is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
"""
_author__ = "Jeremy Bleyer"
__license__ = "CC BY-SA 4.0"
__email__ = "jeremy.bleyer@enpc.fr"

from fenics import *
from mshr import *
import numpy as np
from time import time
from conic_programming import Quad

set_log_level(ERROR)

# Problem parameters
eta, tau0 = Constant(1.), Constant(0.1)
f = Constant(1.)

# Output options
plot_fields = False
output_results = False

# Convergence tolerance and maximum iterations
tol = 1e-8
iter_max = 200
    
# Annulus geometry
path = "interior_point/"
prefix = "annulus_0_04"
domain = Circle(Point(0,0),1.,segments = 100) - Circle(Point(0.04,0.),0.4,segments = 100) - Rectangle(Point(-1,0),Point(1,-1))
mesh = generate_mesh(domain, 50)
# integration measures and cell size
dx = Measure("dx")
dx0 = Measure("dx",metadata={"quadrature_rule":"vertex","quadrature_degree":1})
h_cell = CellVolume(mesh)

# Output file
if output_results:
    results = XDMFFile(mpi_comm_world(),path+prefix+".xdmf")
    results.parameters["flush_output"] = True
    
# Create function space
deg_vel = 1
deg_strain = deg_vel-1
V = FunctionSpace(mesh, "CG", deg_vel)
V_strain = FunctionSpace(mesh,"DG",deg_strain)
# Cone function space
m_cone = 3  # dimension
Vv_strain = VectorFunctionSpace(mesh,"DG",deg_strain,dim=m_cone-1)
Vcone = VectorFunctionSpace(mesh,"DG",deg_strain,dim=m_cone)
# Quadratic Lorentz cone
K = Quad(m_cone)        

# Define functions
u = Function(V,name="Velocity")
sig = Function(Vv_strain,name="stress")
Du = Function(V)
Du_pred = Function(V)
lamb = Function(Vv_strain)
Dlambp = Function(Vv_strain)
Dlamb = Function(Vv_strain)

dv = TrialFunction(V)
v_ = TestFunction(V)
lamb_ = TestFunction(Vv_strain)

# Conic variables
X = Function(Vcone)
S = Function(Vcone)
X.interpolate(Constant((1.,) + (0.,)*(m_cone-1)))
S.interpolate(Constant((1.,) + (0.,)*(m_cone-1)))
DXp = Function(Vcone)
DSp = Function(Vcone)
DX = Function(Vcone)
DS = Function(Vcone)

# Scaling quantities
w = Function(Vcone)
v = Function(Vcone)
theta = Function(V_strain)


# some useful functions
def eps(v):
    return grad(v)
def my_project(v,V,func=True):
    u_ = TestFunction(V)
    r = assemble(1/h_cell*inner(v,u_)*dx0)
    if func:
        u = Function(V)
        u.vector().set_local(r.array())
        u.vector().apply("insert")   
        return u
    else:
        return r.array()
def step_size(x02,x0dx0,dx02,x2,xdx,dx2):
    b = x0dx0-xdx
    a = dx02-dx2
    c = x02-x2
    delta = b**2-a*c
    s = 0*a + 1.
    isol = delta>0
    s[isol] = (-b[isol]-delta[isol]**0.5)/a[isol]
    s[a+2*b+c>0] = 1.
    return float(MPI.min(mpi_comm_world(),s.min()))
def compute_steps(X,DX,S,DS,V0=V_strain,alpha=0.995):
    fd = my_project(dot(K.tail(X),K.tail(X)),V0,func=False)
    fDd = my_project(dot(K.tail(DX),K.tail(DX)),V0,func=False)
    fdDd = my_project(dot(K.tail(X),K.tail(DX)),V0,func=False)
    ft = my_project(X[0],V0,func=False)
    fDt = my_project(DX[0],V0,func=False)
    fl = my_project(S[0],V0,func=False)
    fDl = my_project(DS[0],V0,func=False)
    fl2 = my_project(dot(K.tail(S),K.tail(S)),V0,func=False)
    fDl2 = my_project(dot(K.tail(DS),K.tail(DS)),V0,func=False)
    fl2Dl2 = my_project(dot(K.tail(S),K.tail(DS)),V0,func=False)

    prim_step = step_size(fl**2,fl*fDl,fDl**2,fl2,fl2Dl2,fDl2)
    dual_step = step_size(ft**2,ft*fDt,fDt**2,fd,fdDd,fDd)
    return (alpha*prim_step,alpha*dual_step)
    
# Define boundary conditions
def boundary(x, on_boundary):
    return not(near(x[1],0)) and on_boundary
bc = DirichletBC(V,Constant(0.), boundary)

# mesh volume (S[0]=1)
vol = assemble(S[0]*dx0)
# average complementarity gap
gap = Constant(assemble(dot(X,S)*dx0)/vol)
# centering parameter $\gamma$
centering = Constant(1.)

# Total energy
energy = (0.5*dot(eps(u),eta*eps(u))+tau0*X[0]-dot(f,u))*dx

## DEFINING RESIDUALS
# dual residual
r_dual = (dot(eps(v_),eta*eps(u))-v_*f)*dx + tau0*dot(eps(v_),lamb)*dx0
# primal residual and contribution to reduced system residual
r_prim = dot(lamb_,eps(u)-K.tail(X))*dx0
b_prim = dot(K.F2_tail(w,theta),eps(u)-K.tail(X))
B_prim = dot(tau0*eps(v_),b_prim)*dx0
# centering residual with zero barrier parameter
r_cent = K.circ(v,v)
# centering residual with barrier parameter and quadratic correction from affine step
corr_cent = K.circ(v,v) +  K.circ(K.F(w,theta)*DXp,K.iF(w,theta)*DSp) - centering*gap*K.e0
# contributions to reduced system residual
b_cent = K.F2_tail(w,theta)*K.tail(dot(K.iF(w,theta)*K.imat(v),r_cent))
B_cent = dot(tau0*eps(v_),b_cent)*dx0
b_corr_cent = K.F2_tail(w,theta)*K.tail(dot(K.iF(w,theta)*K.imat(v),corr_cent))
B_corr_cent = dot(tau0*eps(v_),b_corr_cent)*dx0
# residual form for affine step
aff_res = r_dual + B_prim + B_cent
# residual form for correction step
corr_res = (1-centering)*(r_dual + B_prim) + B_corr_cent 

## DEFINING REDUCED BILINEAR FORM FOR NEWTON STEP
J = eta*dot(eps(v_),eps(dv))*dx + tau0*dot(eps(v_),dot(K.F2_tail(w,theta),eps(dv)))*dx0
# Options for linear solver 
solver =  LUSolver("mumps")
solver.parameters["reuse_factorization"] = True
solver.parameters["symmetric"] = True
 
    
# Initial residual norms
R_dual = assemble(r_dual)
bc.apply(R_dual)
R_dual = R_dual.norm('l2')
R_prim = assemble(r_prim)
bc.apply(R_prim)
R_prim = R_prim.norm('l2')
print "Init.        | Res. primal: %.5e | dual: %.5e | gap: %.5e" %(R_prim, R_dual, float(gap))

iteration = 0
curr_time = 0
step = 1.
iter_log = []
while (sqrt(R_dual**2 + R_prim**2) > tol or float(gap) > tol) and step > tol and iteration < iter_max:
    tic = time()
    
    # Current scaling quantities
    w.assign(my_project(K.w(X,S),Vcone))
    v.assign(my_project(K.v(X,S),Vcone))
    theta.assign(my_project(K.theta(X,S),V_strain))
    
    ## PREDICTOR STEP    
    # assemble matrix and reduced residual vector    
    Jac, R = assemble_system(J,-aff_res,bc)
    # solve reduced linear system
    solver.solve(Jac, Du_pred.vector(), R)
    # backsubstitution for auxiliary variable increments
    Dlambp.assign(my_project(dot(K.F2_tail(w,theta),eps(Du_pred)) + b_cent + b_prim,Vv_strain))    
    DSp.assign(my_project(dot(-K.A.T,Dlambp),Vcone))
    Ddp = my_project(eps(u+Du_pred)-K.tail(X),Vv_strain)
    Dtp = my_project(dot(lamb,Ddp)+dot(K.tail(X),Dlambp)-dot(v,v),V_strain)
    DXp.assign(my_project(as_vector([Dtp] + [Ddp[i] for i in range(m_cone-1)]),Vcone))
    # affine step length
    (prim_step,dual_step) = compute_steps(X,DXp,S,DSp)
    step = min(prim_step,dual_step)

    ## EFFICIENCY OF AFFINE PREDICTOR STEP    
    efficiency = 1-step    
    centering.assign(min(0.5,efficiency**2)*efficiency)

    ## CORRECTOR PHASE
    # assemble new residual
    R = assemble(-corr_res)
    bc.apply(R)
    # solving reduced linear system
    solver.solve(Jac,Du.vector(), R)
    # backsubstitution for auxiliary variable increments
    Dlamb.assign(my_project(dot(K.F2_tail(w,theta),eps(Du)) + (1-centering)*(b_prim) + b_corr_cent,Vv_strain)) 
    DS.assign(my_project(dot(-K.A.T,Dlamb),Vcone))
    Dd = my_project(eps(Du)+(1-centering)*(eps(u)-K.tail(X)),Vv_strain)
    Dt = my_project(dot(lamb,Dd)+dot(K.tail(X),Dlamb)-corr_cent[0],V_strain)
    DX.assign(my_project(as_vector([Dt] + [Dd[i] for i in range(m_cone-1)]),Vcone))
    # final step length
    (prim_step,dual_step) = compute_steps(X,DX,S,DS)
    step = min(prim_step,dual_step)
    
    ## FIELDS UPDATE
    u.vector().set_local(u.vector().array() + step*Du.vector().array())
    u.vector().apply("insert")
    X.assign(X + step*DX)
    S.assign(S + step*DS)
    lamb.assign(lamb + step*Dlamb)
    # new residual norms
    R_dual = assemble(r_dual)
    bc.apply(R_dual)
    R_dual = R_dual.norm('l2')
    R_prim = assemble(r_prim)
    bc.apply(R_prim)
    R_prim = R_prim.norm('l2')
    # new complementarity gap
    gap.assign(assemble(dot(X,S)*dx0)/vol)
    
    iteration += 1
    
    ## OUTPUT
    curr_time += time()-tic
    iter_info = [iteration, R_prim, R_dual, float(gap), efficiency, step, curr_time]
    iter_log.append(iter_info)
    print "Iteration %2i | Res. primal: %.5e | dual: %.5e | gap: %.5e | efficiency: %.3e | step size: %.3e | time: %.3e" % tuple(iter_info)
    if plot_fields:
        label = ["u","lambda","gap"]
        plot(u,key=label[0],title=label[0],mode="color")
        plot(lamb,key=label[1],title=label[1])
        plot(dot(X,S),key=label[2],title=label[2],mode="color")  
    if output_results:
        results << (u,float(iteration))
        sig.assign(my_project(eta*eps(u)+tau0*lamb,Vv_strain))        
        results << (sig,float(iteration))
    

print "Finished! Optimal energy:",assemble(energy)
# Compute flow rate
Q = assemble(u*dx)
print "Flow rate:",Q

if output_results:
    np.savetxt(path+prefix+"_iter_log.txt",np.array(iter_log))

interactive()
